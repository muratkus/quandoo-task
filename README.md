# Quandoo automated tests

Automated tests written with Selenium WebDriver API using Java + Maven + TestNG + Page Object Pattern


## Owner

https://github.com/MuratKus && https://bitbucket.org/muratkus/

## Tech stack
This automation project is;

- Written in Java 
- Using Selenium Webdriver for overall functionality
- TestNG framework for test construction and execution
- LogBack for logging 
- Uses Page Object Model as a design pattern for readibility and modularity

## Getting started (and further usage)

1. git clone `git clone https://muratkus@bitbucket.org/muratkus/quandoo-task.git`
2. (Recommended)inside IntelliJ IDEA select 'File' -> 'Open...'
3. choose `pom.xml` from cloned project folder and click OK then - 'Open Existing Project'
4. TIP: make sure to enable auto-import for Maven dependencies
5. define JDK in project settings. That's it!

## How to run the tests

By executing "mvn clean test" through command line in the project directory
OR through the IDE, running the src/test/java/Tests.java 

## Test Results

Test results can be found in the target classes in target/surefire-reports/index.html
Run the report in any browser of your choice and you can view the results.
Each step has sufficient logs printing on the console as well.

If you are executing through the IDE you will be able to see the test cases individually throughout the runtime.

## Project structure:

```
src /
		main /
	java/com/murat / 
	  		data / (For Locators)
	  		pages / (Contains Pages for Page Object Pattern)
	test /
		java/ (scenarios for tests with test steps)
        resources / 
	        	- drivers / (currently only chromedriver)
	        	- logback-test.xml (configuration for ch.qos.logback library for logging)
pom.xml ------ configuration details used by Maven to build the project i.e. Maven dependencies

```

**Enjoy!**
