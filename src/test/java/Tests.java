import com.murat.pages.HoverPage;
import com.murat.pages.LoginPage;
import com.murat.pages.SecurePage;
import com.murat.pages.TablesPage;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertTrue;


public class Tests {
    private WebDriver driver;
    private LoginPage loginPage;
    private SecurePage securePage;
    private HoverPage hoverPage;
    private TablesPage tablesPage;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @BeforeMethod(alwaysRun = true)
    public void setUpTest() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        driver = new ChromeDriver(cap);
        driver.manage().window().setSize(new Dimension(1920, 1080));

        loginPage = new LoginPage(driver);
        securePage = new SecurePage(driver);
        hoverPage = new HoverPage(driver);
        tablesPage = new TablesPage(driver);
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResult) throws Throwable {
        if (testResult.isSuccess()){ logger.info("Scenario Successful");}
        takeScreenshot(testResult);
        tearDown();
    }

    /**
     * Scenario 1 - Login Success
     */
    @Test
    public void loginWithCorrectUserNameAndPass() {
        loginPage
                .open()
                .loginAs("tomsmith", "SuperSecretPassword!");
        assertTrue
                (securePage.isElementVisibleWithXpath("//*[contains(.,'You logged into a secure area')]"),
                        "Success message was not visible");
    }

    /**
     * Scenario 2 - Login Failure; Wrong Username
     */
    @Test
    public void loginWithWrongUserName() {
        loginPage
                .open()
                .loginAs("random", "SuperSecretPassword!");
        assertTrue
                (loginPage.isElementVisibleWithXpath("//*[contains(.,'Your username is invalid')]"),
                        "Failure message was not visible");

    }

    /**
     * Scenario 3 - Login Failure; Correct Username Wrong Password
     */
    @Test
    public void loginWithWrongPassword() {
        loginPage
                .open()
                .loginAs("tomsmith", "SuperWrongPass");
        assertTrue
                (loginPage.isElementVisibleWithXpath("//*[contains(.,'Your password is invalid')]"),
                        "Failure message was not visible");
    }

    /**
     * Scenario 4 - Check Hovering functionality over avatars
     */
    @Test
    public void validateHoverFunctionality(){
        hoverPage
                .open()
                .validateHoverOverAvatars("name: user");
    }

    /**
     * Scenario 5 - Check sorting of table
     */
    @Test
    public void validateAscendingDescending(){
        String columnName = "last-name"; //Declare columnName here that you want to check against
        tablesPage
                .open();
        //If its not Ascending at that moment, on clicking the title it will be ascending
        if (!tablesPage.isAscending(columnName)){
            tablesPage.clickOnLastNameColumn();
            assertTrue(tablesPage.isAscending(columnName),"It is not ascending");
            tablesPage.clickOnLastNameColumn();
            assertTrue(tablesPage.isDescending(columnName), "It is not descending");
        }


    }

    /**
     * Taking the screenshot when test fails.
     *
     * @param testResult
     * @throws IOException
     */
    private void takeScreenshot(ITestResult testResult) throws IOException {
        logger.info("Taking screenshot...");
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("target/screenshots/" + testResult.getName() + ".jpg"));
    }

    public void tearDown() throws Exception {
        if (driver != null) {
            driver.quit();
        }
    }
}
