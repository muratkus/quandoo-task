package com.murat.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractPage<T> {
    protected Logger logger;
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected static final String baseUrl = "http://the-internet.herokuapp.com/";

    protected abstract String getPageUrl();

    AbstractPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30, 30);
        this.logger = LoggerFactory.getLogger(getClass().getName());
        initAnnotations();
    }

    public T open() {
        driver.get(baseUrl + getPageUrl());
        return (T) this;
    }

    private void initAnnotations() {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 15);
        PageFactory.initElements(finder, this);
    }

    public boolean isElementVisibleWithXpath(String xPath) {
        try {
            driver.findElement(By.xpath(xPath));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void moveToElement(WebElement element) {
        Actions builder = new Actions(driver);
        builder.moveToElement(element).perform();
    }

}
