package com.murat.pages;

import com.murat.data.TablesPageProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class TablesPage extends AbstractPage<TablesPage> implements TablesPageProvider {
    private final static String tablePageUrl = "tables";

    @FindBy(how = How.CSS, using = LastName)
    List<WebElement> lastNames;
    @FindBy(how = How.CSS, using = LastNameTitle)
    WebElement lastNameTitle;

    public TablesPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getPageUrl() {
        return tablePageUrl;
    }

    /**
     * Methods
     */

    public boolean isAscending(String columnName) {
        logger.info("Checking if column is in ascending order");
        for (int i = 0; i < lastNames.size() - 1; i++) {
            if (lastNames.get(i).getText().charAt(0) > lastNames.get(i + 1).getText().charAt(0)) {
                logger.info("The column is not in ascending order");
                return false;
            }
        }
        logger.info("The column is in ascending order");
        return true;
    }

    public boolean isDescending(String columnName) {
        logger.info("Checking if column is in descending order");
        for (int i = 0; i < lastNames.size() - 1; i++) {
            if (lastNames.get(i).getText().charAt(0) < lastNames.get(i + 1).getText().charAt(0)) {
                logger.info("The column is not in descending order");
                return false;
            }
        }
        logger.info("The column is in descending order");
        return true;
    }

    public void clickOnLastNameColumn() {
        logger.info("CLicking on Last Name column");
        lastNameTitle.click();
    }

}
