package com.murat.pages;

import com.murat.data.HoverPageProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class HoverPage extends AbstractPage<HoverPage> implements HoverPageProvider {
    WebDriver driver;
    private final static String hoverPageUrl = "hovers";

    @FindBy(how = How.XPATH, using = Avatar)
    List<WebElement> avatars;
    @FindBy(how = How.XPATH, using = AvatarCaption)
    WebElement avatarCaption;

    public HoverPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getPageUrl() {
        return hoverPageUrl;
    }

    /**
     * Methods
     */
    public void validateHoverOverAvatars(String caption) {
        int i = 1;
        for (WebElement avatar : avatars) {
            logger.info("Hovering over avatar");
            moveToElement(avatar);
            assertTrue
                    (isElementVisibleWithXpath(AvatarCaption + caption + i + "')]"),
                            "Not proper caption" + caption + i);
            i++;
        }
    }

}