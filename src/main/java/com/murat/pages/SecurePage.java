package com.murat.pages;

import org.openqa.selenium.WebDriver;

public class SecurePage extends AbstractPage<SecurePage> {
    WebDriver driver;
    private final static String securePageUrl = "secure";
    private final static String AlertSuccess = "//span[@data-alert id='flash'] and contains(@class,'success')";


    protected String getPageUrl() {
        return securePageUrl;
    }

    public SecurePage(WebDriver driver) {
        super(driver);
    }

}