package com.murat.pages;

import com.murat.data.LoginPageProviders;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage extends AbstractPage<LoginPage> implements LoginPageProviders {
    WebDriver driver;
    private final static String loginPageUrl = "login";

    @FindBy(how = How.ID, using = UserName)
    WebElement userName;
    @FindBy(how = How.ID, using = Password)
    WebElement password;
    @FindBy(how = How.XPATH, using = LoginButton)
    WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public String getPageUrl() {
        return loginPageUrl;
    }

    /**
     * Methods
     */
    public void loginAs(String user, String pass) {
        logger.info("Entering credentials username: " + user + " password:" + pass + " clicking on Login Button.");
        userName.sendKeys(user);
        password.sendKeys(pass);
        loginButton.click();
    }
}
