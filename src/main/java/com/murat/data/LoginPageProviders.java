package com.murat.data;

public interface LoginPageProviders {
    /**
     * ID
     */
    String UserName = "username";
    String Password = "password";

    /**
     * Xpath
     */
    String LoginButton = "//*[@class='radius']";
    String AlertFailure = "//span[@data-alert id='flash'] and contains(@class,'error')";

}